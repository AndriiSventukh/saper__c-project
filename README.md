# Saper (C-project)  

Project has been done using simple embedded C-tools, like loops, conditions, arrays, pointers and so on.  
Nothing special, additionally was used only GLFW library (for OpenGl).  
For this game only mouse is needed. At first, player needs to choose area from existing simple clicking on it.  
![Image](game_screens/current area.png)  

Chosen area looks so:  
![Image](game_screens/chosen_area.png)  

The rules are standart for this game, unless fact that hexagonal cell shape is used  
![Image](game_screens/game_process.png)  

If player chooses wrong cell, all bombs explode and game gets failed.  
![Image](game_screens/game_over.png)  

But if all cells marked right, inspiring blinking green indicate WIN!  
![Image](game_screens/win.png)  




