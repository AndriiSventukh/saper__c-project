#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <string.h>

#include "glfw3.h"
#include "glfw3native.h"
#include "glut.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

#include "gamepack.h"
#include "output.h"

gameStatus GameStatus = GameStart;
ifMined IfMinedFlag = freeOfMines;
ifWon IfWon = notYet;
int areaNum = 0;
float implodedColourSet[5][3] =    {{0.98,0.93,0.08},
                                    {0.98,0.76,0.08},
                                    {0.97,0.63,0.08},
                                    {0.98,0.45,0.08},
                                    {0.98,0.29,0.08}};
float deactivatedColourSet[5][3] = {{0.08,0.98,0.9},
                                    {0.08,0.98,0.78},
                                    {0.08,0.98,0.66},
                                    {0.08,0.98,0.52},
                                    {0.08,0.98,0.4}};

void game_engine(hexahedronCell **field
                 ,int mVert
                 ,int nHor
                 ,int* mouseButtonFlag
                 ,double x
                 ,double y
                 ,int* QuitGameFlag)
{

    switch (GameStatus) {
    case Base:
                    makeField(mVert, nHor);
                    GameStatus = GameStart;
        break;
    case GameStart:
                    start_exit_check(x, y, mouseButtonFlag, QuitGameFlag);
                    if ((areaNum = choose_game_area(field, mVert, nHor, x, y, mouseButtonFlag)))
                    {
                        IfMinedFlag = freeOfMines;
                        GameStatus = GameLoop;
                    }
        break;
    case GameLoop:
                    if (IfMinedFlag == freeOfMines)
                        make_mined_field(field, mVert, nHor, areaNum, &IfMinedFlag);
                    draw_game_task(field, mVert, nHor, areaNum);
                    start_exit_check(x, y, mouseButtonFlag, QuitGameFlag);
                    check_game_actions(field, mVert, nHor, x, y, mouseButtonFlag, areaNum);
                    if (IfWon != alreadyWon)
                        check_game_result(field, mVert, nHor);
        break;
    default:
        break;
    }
}

void draw_game_task(hexahedronCell **field,int mVert,int nHor, int aNum) // drawing game task
{
    static GLuint TaskTxtName;
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, TaskTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glEnable(GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    static int colorFlag = 0;
    static int colorIndex = 0;
            if (colorFlag == 0)
                colorIndex++;
            else
                colorIndex--;
            if (colorIndex == 4)
                colorFlag = 1;
            if (colorIndex == 0)
                colorFlag = 0;

    for (int i = 0; i < mVert; ++i)
        for (int j = 0; j < nHor; ++j)
        {
            if (field[i][j].AreaBelonging == aNum)
            {
                glEnable(GL_BLEND);
                switch (field[i][j].CellStatus) {
                case implodedCell:
                    glColor4f(implodedColourSet[colorIndex][0], implodedColourSet[colorIndex][1], implodedColourSet[colorIndex][2], 1.0);
                    break;
                case deactivatedCell:
                    glColor4f(0.0, 0.0, 0.0, 1.0);
                    break;
                case openedCell:
                    if (IfWon == alreadyWon)
                        glColor4f(deactivatedColourSet[colorIndex][0], deactivatedColourSet[colorIndex][1], deactivatedColourSet[colorIndex][2], 1.0);
                    else
                        glColor4f(0.0, 0.0, 0.0, 1.0);
                    break;
                default:
                    glColor4f(field[i][j].ColourSet.R, field[i][j].ColourSet.G, field[i][j].ColourSet.B, field[i][j].ColourSet.Alpha);
                    break;
                }
                glBegin(GL_POLYGON);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x, field[i][j].HexCoord.UpRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x, field[i][j].HexCoord.DownRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x, field[i][j].HexCoord.DownLeft.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x, field[i][j].HexCoord.UpLeft.y);
                glEnd();
                glDisable(GL_BLEND);
            }
        }

        for (int i = 0; i < mVert; ++i)
            for (int j = 0; j < nHor; ++j)
            {
                if (field[i][j].AreaBelonging == aNum)
                {
                glColor3ub(80, 80, 80);
                glLineWidth(0.1f);
                glBegin(GL_LINE_LOOP);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x, field[i][j].HexCoord.UpRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x, field[i][j].HexCoord.DownRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x, field[i][j].HexCoord.DownLeft.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x, field[i][j].HexCoord.UpLeft.y);
                glEnd();
                }
            }

        for (int i = 0; i < mVert; ++i)
            for (int j = 0; j < nHor; ++j)
            {
                if (field[i][j].AreaBelonging == aNum)
                {
                    glColor3ub(255, 20, 40);
                    glLineWidth(2);
                    if (i == 0 || (j == (nHor-1) && i%2 != 0) || (field[i-1][j+i%2].AreaBelonging != aNum))
                    {
                        glBegin(GL_LINES);
                            glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y-1.0);
                            glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x+1.0, field[i][j].HexCoord.UpRight.y-1.0);
                        glEnd();
                    }
                    if ((j == (nHor-1)) || (field[i][j+1].AreaBelonging != aNum))
                    {
                        glBegin(GL_LINES);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x+1.0, field[i][j].HexCoord.UpRight.y-1.0);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x+1.0, field[i][j].HexCoord.DownRight.y+1.0);
                        glEnd();
                    }
                    if (i == (mVert-1) || (j == (nHor-1) && i%2 != 0) || (field[i+1][j+i%2].AreaBelonging != aNum))
                    {
                        glBegin(GL_LINES);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x+1.0, field[i][j].HexCoord.DownRight.y+1.0);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y+1.0);
                        glEnd();
                    }
                    if (i == (nHor-1) || (j == 0 && i%2 == 0) || (field[i+1][j-(i+1)%2].AreaBelonging != aNum))
                    {
                        glBegin(GL_LINES);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y+1.0);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x-1.0, field[i][j].HexCoord.DownLeft.y+1.0);
                        glEnd();
                    }
                    if (j == 0 || (field[i][j-1].AreaBelonging != aNum))
                    {
                        glBegin(GL_LINES);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x-1.0, field[i][j].HexCoord.DownLeft.y+1.0);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x-1.0, field[i][j].HexCoord.UpLeft.y-1.0);
                        glEnd();
                    }
                    if (i == 0 || (j == 0 && i%2 == 0) || (field[i-1][j-(i+1)%2].AreaBelonging != aNum))
                    {
                        glBegin(GL_LINES);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x-1.0, field[i][j].HexCoord.UpLeft.y-1.0);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y-1.0);
                        glEnd();
                    }
                }
            }

        for (int i = 0; i < mVert; ++i)
            for (int j = 0; j < nHor; ++j)
            {
                if (field[i][j].CellStatus == markedFreeCell || field[i][j].CellStatus == markedMinedCell)
                {
                    glEnable(GL_LINE_SMOOTH);
                    glColor4f(0.98, 0.65, 0.1, 1.0);
                    glBegin(GL_POLYGON);
                        glTexCoord2d(0,0); glVertex2d(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y+4.0);
                        glTexCoord2d(0,0); glVertex2d(field[i][j].HexCoord.UpRight.x-3.0, field[i][j].HexCoord.UpRight.y+2.0);
                        glTexCoord2d(0,0); glVertex2d(field[i][j].HexCoord.DownRight.x-3.0, field[i][j].HexCoord.DownRight.y-2.0);
                        glTexCoord2d(0,0); glVertex2d(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y-4.0);
                        glTexCoord2d(0,0); glVertex2d(field[i][j].HexCoord.DownLeft.x+3.0, field[i][j].HexCoord.DownLeft.y-2.0);
                        glTexCoord2d(0,0); glVertex2d(field[i][j].HexCoord.UpLeft.x+3.0, field[i][j].HexCoord.UpLeft.y+2.0);
                    glEnd();
                }
            }

        glColor3ub(250, 240, 30);
        for (int i = 0; i < mVert; ++i)
            for (int j = 0; j < nHor; ++j)
            {
                if (field[i][j].AreaBelonging == aNum && field[i][j].CellStatus == openedCell)
                {
                    glRasterPos2d(field[i][j].HexCoord.UpLeft.x+4.0, field[i][j].HexCoord.UpLeft.y+7.5);
                    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, field[i][j].surroundings+48);
                }
            }
}

int choose_game_area(hexahedronCell **field, int m, int n, double x, double y, int *mButton) // choosing game area
{
    static GLuint AreaTxtName;
    int aNum = 0;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
    {
        if (    field[i][j].HexCoord.DownLeft.x -1.0 <=x
                && field[i][j].HexCoord.DownRight.x + 1.0 >= x
                && (field[i][j].HexCoord.UpLeft.y - (double)(field[i][j].HexCoord.UpLeft.y - field[i][j].HexCoord.Up.y + 2.0)/2) <= y
                && (field[i][j].HexCoord.DownLeft.y + (double)(field[i][j].HexCoord.Down.y - field[i][j].HexCoord.DownLeft.y + 2.0)/2) >= y)
        {
            aNum = field[i][j].AreaBelonging;
        }
    }
    if (aNum != 0)
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture (GL_TEXTURE_2D, AreaTxtName);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
        glEnable(GL_ALPHA_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
        {
            if (field[i][j].AreaBelonging == aNum)
            {
                glColor3ub(255, 20, 40);
                glLineWidth(10);
                if (i == 0 || (j == (n-1) && i%2 != 0) || (field[i-1][j+i%2].AreaBelonging != aNum))
                {
                    glBegin(GL_LINES);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y-1);
                        glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x+1, field[i][j].HexCoord.UpRight.y-1);
                    glEnd();
                }
                if ((j == (n-1)) || (field[i][j+1].AreaBelonging != aNum))
                {
                    glBegin(GL_LINES);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x+1, field[i][j].HexCoord.UpRight.y-1);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x+1, field[i][j].HexCoord.DownRight.y+1);
                    glEnd();
                }
                if (i == (m-1) || (j == (n-1) && i%2 != 0) || (field[i+1][j+i%2].AreaBelonging != aNum))
                {
                    glBegin(GL_LINES);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x+1, field[i][j].HexCoord.DownRight.y+1);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y+1);
                    glEnd();
                }
                if (i == (n-1) || (j == 0 && i%2 == 0) || (field[i+1][j-(i+1)%2].AreaBelonging != aNum))
                {
                    glBegin(GL_LINES);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y+1);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x-1, field[i][j].HexCoord.DownLeft.y+1);
                    glEnd();
                }
                if (j == 0 || (field[i][j-1].AreaBelonging != aNum))
                {
                    glBegin(GL_LINES);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x-1, field[i][j].HexCoord.DownLeft.y+1);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x-1, field[i][j].HexCoord.UpLeft.y-1);
                    glEnd();
                }
                if (i == 0 || (j == 0 && i%2 == 0) || (field[i-1][j-(i+1)%2].AreaBelonging != aNum))
                {
                    glBegin(GL_LINES);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x-1, field[i][j].HexCoord.UpLeft.y-1);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y-1);
                    glEnd();
                }
            }
        }
    }
    if (*mButton == GLFW_MOUSE_BUTTON_LEFT && aNum != 0)
    {
        *mButton = -1;
        return aNum;
    }
    else return 0;
}

void make_mined_field(hexahedronCell **field,int mVert,int nHor, int aNum, ifMined *IfMinedFlad) // filling game field with mines
{
    int areaCellCounter = 0;
    for (int i = 0; i < mVert; ++i)
        for (int j = 0; j < nHor; ++j)
    {
        if (field[i][j].AreaBelonging == aNum)
            areaCellCounter++;
    }

    hexahedronCell **minedCells;
    minedCells = (hexahedronCell**)calloc(areaCellCounter, sizeof(hexahedronCell*));
    int iCell=0;
    for (int i = 0; i < mVert && iCell < areaCellCounter; ++i)
        for (int j = 0; j < nHor && iCell < areaCellCounter; ++j)
        {
            if (field[i][j].AreaBelonging == aNum)
            {
                minedCells[iCell] = *(field+i) + j;
                iCell++;
            }
        }

    time_t t;
    srand((unsigned) time(&t));
    float GameLevel = ((float)(5 + rand()%21))/100;
    int MinedCellsAmount = (int)(areaCellCounter * GameLevel);
    while (MinedCellsAmount)
    {
        int i = rand()%areaCellCounter;
        if ((minedCells[i])->AreaBelonging == aNum && (*minedCells[i]).CellStatus != minedCell)
        {
            (*minedCells[i]).CellStatus = minedCell;
            MinedCellsAmount--;
        }
    }
    free(minedCells);
    *IfMinedFlad = mined;

    int surroundingCounter;
    for (int i = 1; i < mVert-1; ++i)
        for (int j = 1; j < nHor-1; ++j)
        {
            if (field[i][j].AreaBelonging == aNum && field[i][j].CellStatus != minedCell)
            {
                surroundingCounter = 0;
                if (field[i-1][j -(i+1)%2].CellStatus == minedCell)
                    surroundingCounter++;
                if (field[i-1][j+i%2].CellStatus == minedCell)
                    surroundingCounter++;
                if (field[i][j+1].CellStatus == minedCell)
                    surroundingCounter++;
                if (field[i+1][j+i%2].CellStatus == minedCell)
                    surroundingCounter++;
                if (field[i+1][j-(i+1)%2].CellStatus == minedCell)
                    surroundingCounter++;
                if (field[i][j-1].CellStatus == minedCell)
                    surroundingCounter++;
                field[i][j].surroundings = surroundingCounter;
            }
        }
}

void check_game_actions(hexahedronCell **field, int m, int n, double x, double y, int *mButton, int aNum) // checking gamer's actions
{

    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
    {
        if (    field[i][j].HexCoord.DownLeft.x -1.0 <=x
                && field[i][j].HexCoord.DownRight.x + 1.0 >= x
                && (field[i][j].HexCoord.UpLeft.y - (double)(field[i][j].HexCoord.UpLeft.y - field[i][j].HexCoord.Up.y + 2.0)/2) <= y
                && (field[i][j].HexCoord.DownLeft.y + (double)(field[i][j].HexCoord.Down.y - field[i][j].HexCoord.DownLeft.y + 2.0)/2) >= y
                && field[i][j].AreaBelonging == aNum)
        {
            if (*mButton == GLFW_MOUSE_BUTTON_RIGHT)
            {
                switch (field[i][j].CellStatus) {
                case minedCell:
                    field[i][j].CellStatus = markedMinedCell;
                    break;
                case freeCell:
                    field[i][j].CellStatus = markedFreeCell;
                    break;
                case markedFreeCell:
                    field[i][j].CellStatus = freeCell;
                    break;
                case markedMinedCell:
                    field[i][j].CellStatus = minedCell;
                    break;
                default:
                    break;
                }
            }
            else
            {
                if ( *mButton == GLFW_MOUSE_BUTTON_LEFT)
                {
                     switch (field[i][j].CellStatus) {
                     case minedCell:
                     {
                         for (int i = 0; i < m; ++i)
                             for (int j = 0; j < n; ++j)
                             {
                                 if (field[i][j].CellStatus == minedCell || field[i][j].CellStatus == markedMinedCell)
                                    field[i][j].CellStatus = implodedCell;
                                 else
                                     field[i][j].CellStatus = openedCell;
                             }
                     }
                         break;
                     case freeCell:
                     {
                         field[i][j].CellStatus = openedCell;
                                 if (field[i-1][j -(i+1)%2].CellStatus == freeCell)
                                     field[i-1][j -(i+1)%2].CellStatus = openedCell;
                                 if (field[i-1][j+i%2].CellStatus == freeCell)
                                     field[i-1][j+i%2].CellStatus = openedCell;
                                 if (field[i][j+1].CellStatus == freeCell)
                                     field[i][j+1].CellStatus = openedCell;
                                 if (field[i+1][j+i%2].CellStatus == freeCell)
                                     field[i+1][j+i%2].CellStatus = openedCell;
                                 if (field[i+1][j-(i+1)%2].CellStatus == freeCell)
                                     field[i+1][j-(i+1)%2].CellStatus = openedCell;
                                 if (field[i][j-1].CellStatus == freeCell)
                                     field[i][j-1].CellStatus = openedCell;
                     }
                         break;
                     default:
                         break;
                     }
                }
            }
        }
    }
    *mButton = -1;
}

void check_game_result(hexahedronCell **field, int m, int n) // checking the game status
{
    int inaproprCellCounter = 0;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (field[i][j].CellStatus == minedCell || field[i][j].CellStatus == markedFreeCell || field[i][j].CellStatus == implodedCell)
               inaproprCellCounter++;
    if (inaproprCellCounter == 0 && IfMinedFlag == mined && GameStatus == GameLoop)
    {
        IfWon = alreadyWon;
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
            {
                if (field[i][j].CellStatus == markedMinedCell)
                    field[i][j].CellStatus = deactivatedCell;
                if (field[i][j].CellStatus == freeCell)
                    field[i][j].CellStatus = openedCell;
            }
    }
}

void start_exit_check(double x, double y, int *mouseButtonFlag, int* QuitGameFlag) // checkuing pressing main buttons
{

    if ( *mouseButtonFlag == GLFW_MOUSE_BUTTON_LEFT)
    {
        if (x > 1195 && x < 1330 && y > 620 && y < 665)
        {
            *QuitGameFlag = 1;
            *mouseButtonFlag = -1;
        }
    }

    if ( *mouseButtonFlag == GLFW_MOUSE_BUTTON_LEFT)
    {
        if (x > 25 && x < 165 && y > 620 && y < 665)
        {
            GameStatus = Base;
            *mouseButtonFlag = -1;
            IfWon = notYet;
            IfMinedFlag = freeOfMines;;
            areaNum = 0;
        }
    }
}
