#ifndef OUTPUT_H
#define OUTPUT_H

#define SCREEN_WIDTH        1350
#define SCREEN_HEIGHT       690
#define BUTTON_WIDTH        258
#define BUTTON_HEIGHT       79

typedef unsigned char stbi_uc;

typedef struct{
    double x;
    double y;
}coordinates;

typedef struct{
    float R;
    float G;
    float B;
    float Alpha;
}colourSet;

typedef struct{
    coordinates Up;
    coordinates UpRight;
    coordinates DownRight;
    coordinates Down;
    coordinates DownLeft;
    coordinates UpLeft;
}hexahedronCoordinates;

typedef enum{
     freeCell
    ,minedCell
    ,markedMinedCell
    ,markedFreeCell
    ,openedCell
    ,implodedCell
    ,deactivatedCell
}cellStatus;

typedef struct{
    hexahedronCoordinates HexCoord;
    colourSet ColourSet;
    cellStatus CellStatus;
    int AreaBelonging;
    int surroundings;
}hexahedronCell;

typedef enum{
     Off
    ,On
}serviceFlag;

void processingEngine_Ogl();
void key_click_clb (GLFWwindow*, int, int, int, int);
void resize_clb (GLFWwindow*, int, int);
void cursor_position_callback(GLFWwindow*, double, double);
void mouse_button_callback(GLFWwindow*, int, int, int);
void draw_game_background();
void draw_main_buttons();
void makeField(int, int);
void draw_info();

#endif // OUTPUT_H
