#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

#include "glfw3.h"
#include "glfw3native.h"
#include "glut.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb.h"
#include "stb_image.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

#include "output.h"
#include "servicepack.h"
#include "gamepack.h"

serviceFlag ServiceFlag = Off;
int n = 135;
int m = 69;
int nHor = 84;
int mVert = 49;
int nHorShift = 0;
int mVertShift = 1;
int cellDistHor = 2;
int cellDistVer = 2;
int cellWidth = 14;
int cellHeight = 16;
double xCursor =0.0;
double yCursor = 0.0;
int QuitGameFlag = 0;
int buttonFlag = -1;
int buttonFlagMutex = 0;
int mouseButtonFlag = -1;
int mouseButtonFlagMutex = 0;
hexahedronCell **field = NULL;

void processingEngine_Ogl()
{

    // switching the console off
    HWND myConsole = GetConsoleWindow();
    ShowWindow(myConsole, 0);

    makeField(mVert, nHor);

    GLFWwindow* window;
    if (!glfwInit())
        return;
    window = glfwCreateWindow(n * 10, m * 10, "SAPPER, super-puper-mega-cool Game", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return;
    }
    glfwSetWindowPos(window,8,30);
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_click_clb);
    glfwSetWindowSizeCallback (window, resize_clb);
    glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    glViewport(0, 0, (n * 10), (m * 10));
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, (GLsizei)(n * 10), (GLsizei)(m * 10), 0, 0, 1);
    glfwSwapInterval(1);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_TEXTURE_2D);


    // main loop for rendering
    while (!glfwWindowShouldClose(window))
    {
        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glfwSetWindowSize(window, n*10, m*10);
        glViewport(0, 0, (n * 10), (m * 10));
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, (GLsizei)(n * 10), (GLsizei)(m * 10), 0, 0, 1);

         draw_game_background();
         draw_main_buttons ();

         switch (ServiceFlag) {
         case On:   {
                        field_creator_service(  field
                                                ,mVert
                                                ,nHor
                                                ,&buttonFlag
                                                ,&buttonFlagMutex
                                                ,&mouseButtonFlag
                                                ,&mouseButtonFlagMutex
                                                ,xCursor
                                                ,yCursor
                                                ,&ServiceFlag);
                    }
             break;
         case Off:  {
                        game_engine(             field
                                                ,mVert
                                                ,nHor
                                                ,&mouseButtonFlag
                                                ,xCursor
                                                ,yCursor
                                                ,&QuitGameFlag);
                    }
             break;
         default:
             break;
         }
         draw_info();

        Sleep(10);
        glfwSwapBuffers(window);
        glfwPollEvents();

        if (QuitGameFlag)
        {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }
    }

    glfwTerminate();
    if (field != NULL)
    {
        m = _msize(field)/sizeof(hexahedronCell**);
        for (int i = 0; i < m; ++i)
            free(*(field + i));
        free(field);
    }
    return;
}

void resize_clb (GLFWwindow *apWindow, int aWidth, int aHeight)
{
    if (aWidth != n * 10 || aHeight != m *10)
    {
        glfwSetWindowSize(apWindow, n*10, m*10);
        glViewport(0, 0, (n * 10), (m * 10));
    }
}

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    xCursor = xpos;
    yCursor = ypos;
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (action == GLFW_RELEASE && mouseButtonFlagMutex == 0)
        {
            mouseButtonFlag = button;
        }
}

void key_click_clb(GLFWwindow *window, int Key, int ScanCode, int Action, int Mods)
{
    if(Action == GLFW_PRESS || Action == GLFW_REPEAT)
       {
           buttonFlag = Key;
       }
}

void makeField(int mVert, int nHor) // creating game field from file or from scratch
{
    int i = 0;
    int j = 0;

    if (field != NULL)
    {
        for (int i = 0; i < mVert; ++i)
            free(field[i]);
        free(field);
    }

    field = (hexahedronCell**)malloc((mVert) * sizeof(hexahedronCell*));
    for (i = 0; i < mVert; i++)
         *(field + i) = (hexahedronCell*)calloc((nHor), sizeof(hexahedronCell));

    FILE *fp;
    if ((fp = fopen("field.dat", "rb")))
    {
        for (i = 0; i < mVert; i++)
            for (j = 0; j < nHor; j++)
            {
                fread((*(field + i) + j), sizeof(hexahedronCell), 1, fp);
            }
        fclose(fp);
    }
    else
    {
        for (i = 0; i < mVert; i++)
            for (j = 0; j < nHor; j++)
            {
                (*(*(field + i) + j)).HexCoord.Up.x=(double)(nHorShift+(j*(cellWidth + cellDistHor))+7 + (7+cellDistHor/2)*(i%2));
                (*(*(field + i) + j)).HexCoord.UpRight.x=(double)(nHorShift+(j*(cellWidth + cellDistHor)+cellWidth) + (7+cellDistHor/2)*(i%2));
                (*(*(field + i) + j)).HexCoord.DownRight.x=(double)(nHorShift+(j*(cellWidth + cellDistHor)+cellWidth) + (7+cellDistHor/2)*(i%2));
                (*(*(field + i) + j)).HexCoord.Down.x=(double)(nHorShift+(j*(cellWidth + cellDistHor))+7 + (7+cellDistHor/2)*(i%2));
                (*(*(field + i) + j)).HexCoord.DownLeft.x=(double)(nHorShift+(j*(cellWidth + cellDistHor)) + (7+cellDistHor/2)*(i%2));
                (*(*(field + i) + j)).HexCoord.UpLeft.x=(double)(nHorShift+(j*(cellWidth + cellDistHor)) + (7+cellDistHor/2)*(i%2));

                (*(*(field + i) + j)).HexCoord.Up.y=(double)(mVertShift+(i*(cellHeight+cellDistVer)) - 4*i);
                (*(*(field + i) + j)).HexCoord.UpRight.y=(double)(mVertShift+(i*(cellHeight+cellDistVer))+4 - 4*i);
                (*(*(field + i) + j)).HexCoord.DownRight.y=(double)(mVertShift+(i*(cellHeight+cellDistVer))+4+8 - 4*i);
                (*(*(field + i) + j)).HexCoord.Down.y=(double)(mVertShift+(i*(cellHeight+cellDistVer))+cellHeight - 4*i);
                (*(*(field + i) + j)).HexCoord.DownLeft.y=(double)(mVertShift+(i*(cellHeight+cellDistVer))+4+8 - 4*i);
                (*(*(field + i) + j)).HexCoord.UpLeft.y=(double)(mVertShift+(i*(cellHeight+cellDistVer))+4 - 4*i);

                (*(*(field + i) + j)).ColourSet.R=0.1;
                (*(*(field + i) + j)).ColourSet.G=0.1;
                (*(*(field + i) + j)).ColourSet.B=0.1;
                (*(*(field + i) + j)).ColourSet.Alpha=0.3;
                (*(*(field + i) + j)).AreaBelonging = 0;
                (*(*(field + i) + j)).CellStatus = freeCell;
            }
    }
}

void draw_game_background() // drawing map, used as the game background
{
    static int InitDrawBKGRND = 1;
    static GLuint TxtName;
    static stbi_uc *DataPtr = NULL;

    if (InitDrawBKGRND)
    {
          int width, height, nrChan;
        DataPtr = stbi_load("europe.jpg", &width, &height, &nrChan, STBI_rgb_alpha);
        glGenTextures(1, &TxtName);
        glBindTexture (GL_TEXTURE_2D, TxtName);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, DataPtr);
        InitDrawBKGRND = 0;
    }

    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, TxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glColor3ub(0, 0, 0);
    glBegin(GL_QUADS);
        glTexCoord2d(0,0); glVertex2i(0, 0);
        glTexCoord2d(0,1); glVertex2i(0, m * 10);
        glTexCoord2d(1,1); glVertex2i(n * 10, m * 10);
        glTexCoord2d(1,0); glVertex2i(n * 10, 0);
    glEnd();
}

void draw_main_buttons() // drawing main buttons
{
    static int InitDrawIm = 1;
    static GLuint TxtName [2];
    static stbi_uc *DataPtr[2] = {NULL};

    if (InitDrawIm)
    {
        int width, height, nrChan;
        DataPtr[0] = stbi_load("buttonstart.png",&width, &height, &nrChan, STBI_rgb_alpha);
        glGenTextures(1, &TxtName[0]);
        glBindTexture (GL_TEXTURE_2D, TxtName[0]);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, DataPtr[0]);

        DataPtr[1] = stbi_load("buttonquit.png", &width, &height, &nrChan, STBI_rgb_alpha);
        glGenTextures(1, &TxtName[1]);
        glBindTexture (GL_TEXTURE_2D, TxtName[1]);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, DataPtr[1]);
        InitDrawIm = 0;
    }

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
              glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, TxtName[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glBegin(GL_QUADS);
        glTexCoord2d(0,0); glVertex2i(10, 600);
        glTexCoord2d(0,1); glVertex2i(10, 690);
        glTexCoord2d(1,1); glVertex2i(180, 690);
        glTexCoord2d(1,0); glVertex2i(180, 600);
    glEnd();
    glFlush();
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
              glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, TxtName[1]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glBegin(GL_QUADS);
        glTexCoord2d(0,0); glVertex2i(1190, 620);
        glTexCoord2d(0,1); glVertex2i(1190, 680);
        glTexCoord2d(1,1); glVertex2i(1340, 680);
        glTexCoord2d(1,0); glVertex2i(1340, 620);
    glEnd();
    glFlush();
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
}


void draw_info() // drawing info
{
    static int  escPressedFlag = 0;
    static int InitDrawInfo = 1;
    static GLuint TxtName2;
    static stbi_uc *DataPtr2 = NULL;

    if (InitDrawInfo)
    {
        int width, height, nrChan;
        DataPtr2 = stbi_load("info.png",&width, &height, &nrChan, STBI_rgb_alpha);
        glGenTextures(1, &TxtName2);
        glBindTexture (GL_TEXTURE_2D, TxtName2);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, DataPtr2);
        InitDrawInfo = 0;
    }

    if (buttonFlag == GLFW_KEY_ESCAPE)
    {
            escPressedFlag++;
            buttonFlag = -1;
    }

    if (escPressedFlag % 2)
    {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                  glEnable(GL_BLEND);
        glEnable(GL_TEXTURE_2D);
        glBindTexture (GL_TEXTURE_2D, TxtName2);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glBegin(GL_QUADS);
            glTexCoord2d(0,0); glVertex2i(300, 70);
            glTexCoord2d(0,1); glVertex2i(300, 630);
            glTexCoord2d(1,1); glVertex2i(1000, 630);
            glTexCoord2d(1,0); glVertex2i(1000, 70);
        glEnd();
        glFlush();
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }
}
