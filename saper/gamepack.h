#ifndef GAMEPACK_H
#define GAMEPACK_H

#include "output.h"

typedef enum{
     Base
    ,GameStart
    ,GameLoop
}gameStatus;

typedef enum{
     freeOfMines
    ,mined
}ifMined;

typedef enum{
     notYet
    ,alreadyWon
}ifWon;

void game_engine(hexahedronCell**, int, int, int*, double, double, int*);
void draw_game_task(hexahedronCell**, int, int, int);
int choose_game_area(hexahedronCell**, int, int, double, double, int*);
void make_mined_field(hexahedronCell**, int,int, int, ifMined*);
void check_game_actions(hexahedronCell**, int, int, double, double, int*, int);
void check_game_result(hexahedronCell**, int, int);
void start_exit_check(double, double, int*, int*);


#endif // GAMEPACK_H
