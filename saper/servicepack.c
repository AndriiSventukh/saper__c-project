#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

#include "glfw3.h"
#include "glfw3native.h"
#include "glut.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

#include "servicepack.h"
#include "output.h"

void field_creator_service(  hexahedronCell **field
                            ,int mVert
                            ,int nHor
                            ,int* buttonFlag
                            ,int* buttonFlagMutex
                            ,int* mouseButtonFlag
                            ,int* mouseButtonFlagMutex
                            ,double xCursor
                            ,double yCursor
                            ,serviceFlag *serviceFlag)
{
    static int saveFileFlag = 0;
    static int areaNumber = 1;

    if ((*buttonFlag) == GLFW_KEY_S)
    {
        saveFileFlag++;
    }

    if (!saveFileFlag)
    {
        if ((*buttonFlag) == GLFW_KEY_N)
            areaNumber++;
        if (*mouseButtonFlag == GLFW_MOUSE_BUTTON_LEFT || *mouseButtonFlag == GLFW_MOUSE_BUTTON_RIGHT)
            check_cell_param(field, mVert, nHor, xCursor, yCursor, areaNumber, *mouseButtonFlag);

        *buttonFlag = -1;
        *buttonFlagMutex = 0;
        *mouseButtonFlag = -1;
        *mouseButtonFlagMutex = 0;

        draw_service_field(field, mVert, nHor);
    }
    else
    {
        clearFieldServiceColours(field, mVert, nHor);
        saveFieldAreasToFile(field, mVert, nHor);
        *buttonFlag = -1;
        *buttonFlagMutex = 0;
        *mouseButtonFlag = -1;
        *mouseButtonFlagMutex = 0;
        *serviceFlag = Off;
    }
}

void draw_service_field(hexahedronCell **field,int mVert,int nHor)
{
    static GLuint *ServiceTxtName;
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, *ServiceTxtName);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    glEnable(GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < mVert; ++i)
        for (int j = 0; j < nHor; ++j)
        {
            {
                glEnable(GL_BLEND);
                glColor4f(field[i][j].ColourSet.R, field[i][j].ColourSet.G, field[i][j].ColourSet.B, field[i][j].ColourSet.Alpha);
                glBegin(GL_POLYGON);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x, field[i][j].HexCoord.UpRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x, field[i][j].HexCoord.DownRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x, field[i][j].HexCoord.DownLeft.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x, field[i][j].HexCoord.UpLeft.y);
                glEnd();
                glDisable(GL_BLEND);
            }
        }
        for (int i = 0; i < mVert; ++i)
            for (int j = 0; j < nHor; ++j)
        {
            {
                glColor3ub(80, 80, 80);
                glLineWidth(0.1f);
                glBegin(GL_LINE_LOOP);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Up.x, field[i][j].HexCoord.Up.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpRight.x, field[i][j].HexCoord.UpRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownRight.x, field[i][j].HexCoord.DownRight.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.Down.x, field[i][j].HexCoord.Down.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.DownLeft.x, field[i][j].HexCoord.DownLeft.y);
                    glTexCoord2d(0,0); glVertex2f(field[i][j].HexCoord.UpLeft.x, field[i][j].HexCoord.UpLeft.y);
                glEnd();
            }
        }
}

void check_cell_param(hexahedronCell **field, int m, int n, double x, double y, int aNum, int button)
{
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
    {
        if (    field[i][j].HexCoord.DownLeft.x -1.0 <=x
                && field[i][j].HexCoord.DownRight.x + 1.0 >= x
                && (field[i][j].HexCoord.UpLeft.y - (double)(field[i][j].HexCoord.UpLeft.y - field[i][j].HexCoord.Up.y + 2.0)/2) <= y
                && (field[i][j].HexCoord.DownLeft.y + (double)(field[i][j].HexCoord.Down.y - field[i][j].HexCoord.DownLeft.y + 2.0)/2) >= y)
        {
            if (button == GLFW_MOUSE_BUTTON_LEFT && field[i][j].AreaBelonging != aNum)
            {
                field[i][j].AreaBelonging = aNum;
                field[i][j].ColourSet.Alpha =1;
                if (field[i][j].ColourSet.R + (double)(0.2 * aNum) <=1)
                {
                    field[i][j].ColourSet.R += (double)(0.2 * aNum);
                }
                else
                    if (field[i][j].ColourSet.G + (double)((0.2 * aNum)-0.8) <=1)
                    {
                        field[i][j].ColourSet.G += (double)((0.2 * aNum)-0.8);
                    }
                    else
                        if (field[i][j].ColourSet.B + (double)((0.2 * aNum)-1.6) <= 1)
                        {
                            field[i][j].ColourSet.B += (double)((0.2 * aNum)-1.6);
                        }
            }
            else if (button == GLFW_MOUSE_BUTTON_RIGHT)
            {
                field[i][j].AreaBelonging = 0;
                (*(*(field + i) + j)).ColourSet.R=0.1;
                (*(*(field + i) + j)).ColourSet.G=0.1;
                (*(*(field + i) + j)).ColourSet.B=0.1;
                (*(*(field + i) + j)).ColourSet.Alpha=0.3;
            }
        }
    }
}

void clearFieldServiceColours(hexahedronCell **field, int m, int n)
{
    int i = 0;
    int j = 0;

    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
        {
            (*(*(field + i) + j)).ColourSet.R = 0.5;
            (*(*(field + i) + j)).ColourSet.G = 0.5;
            (*(*(field + i) + j)).ColourSet.B = 0.5;
            (*(*(field + i) + j)).ColourSet.Alpha=1;
        }
}

void saveFieldAreasToFile(hexahedronCell **field, int m, int n)
{
    FILE *fp;
    int i = 0;
    int j = 0;
    if ((fp = fopen("field.dat", "wb+")))
    {
        for (i = 0; i < m; i++)
            for (j = 0; j < n; j++)
            {
                fwrite((*(field + i) + j), sizeof(hexahedronCell), 1, fp);
            }
    }
    fclose(fp);
}
