#include <windows.h>
#include "glfw3.h"
#include "glfw3native.h"
#include "glut.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

#include "output.h"

int main(void)
{
    processingEngine_Ogl();
    return 0;
}

