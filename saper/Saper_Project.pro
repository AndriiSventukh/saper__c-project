TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_LFLAGS += -static-libgcc

QMAKE_CFLAGS += -std=c11
        QT += opengl
        LIBS += -lopengl32

SOURCES += main.c \
    output.c \
    servicepack.c \
    gamepack.c

win32: LIBS += -L$$PWD/ -lglfw3
win32: LIBS += -L$$PWD/ -lglfw3dll
win32: LIBS += -L$$PWD/ -lglut32
win32: LIBS += -L$$PWD/ -lglaux
win32: LIBS += -L$$PWD/ -lgdi32


HEADERS += \
    glfw3.h \
    glfw3native.h \
    glut.h \
    glaux.h \
    stb_image.h \
    stb.h \
    output.h \
    servicepack.h \
    gamepack.h
