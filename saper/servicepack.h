#ifndef SERVICEPACK_H
#define SERVICEPACK_H

#include "output.h"

void field_creator_service(hexahedronCell**, int, int, int*, int*, int*, int*, double, double, serviceFlag*);
void draw_service_field(hexahedronCell**, int, int);
void check_cell_param(hexahedronCell**, int, int, double, double, int, int);
void clearFieldServiceColours(hexahedronCell**, int, int);
void saveFieldAreasToFile(hexahedronCell**, int, int);

#endif // SERVICEPACK_H
